/*
 * Szklanka.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef SZKLANKA_HPP_
#define SZKLANKA_HPP_

class Szklanka {
private:
	static int cena;
	int pojemnosc;
public:
	static int getCena();

	static void setCena(int cc);

	Szklanka(int poj) :
			pojemnosc(poj) {
}

int getPojemnosc() const;

void setPojemnosc(int pojemnosc);
};

#endif /* SZKLANKA_HPP_ */
