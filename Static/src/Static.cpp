//============================================================================
// Name        : Static.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Szklanka.hpp"
using namespace std;

int main() {

	cout << "Cena bez obiektu = " << Szklanka::getCena() << endl;

	Szklanka::setCena(1000);
	cout << "Cena bez obiektu = " << Szklanka::getCena() << endl;

	Szklanka szkl1(100);
	Szklanka szkl2(200);

	cout << "Cena przed 1 = " << szkl1.getCena() << " 2 = " << szkl2.getCena() << endl;

	szkl1.setCena(10);
	cout << "Cena przed 1 = " << szkl1.getCena() << " 2 = " << szkl2.getCena() << endl;

	Szklanka szkl3 (300);
	cout << "Cena po 1 konstruktorze = " << szkl1.getCena() << " 2 = " << szkl2.getCena() << endl;


	return 0;
}
