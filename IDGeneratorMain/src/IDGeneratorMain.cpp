//============================================================================
// Name        : IDGeneratorMain.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Generator.hpp"

using namespace std;

int main() {

	cout << IDGenerator::getNextID() << endl;
	cout << IDGenerator::getNextID() << endl;
	cout << IDGenerator::getNextID() << endl;
	cout << IDGenerator::getNextID() << endl;
	cout << IDGenerator::getNextID() << endl;

	return 0;
}
