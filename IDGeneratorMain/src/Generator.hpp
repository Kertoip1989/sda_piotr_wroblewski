/*
 * Generator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef GENERATOR_HPP_
#define GENERATOR_HPP_

class IDGenerator
{
private:
	static int id;
public:
	IDGenerator();

	static int getNextID();

};



#endif /* GENERATOR_HPP_ */
