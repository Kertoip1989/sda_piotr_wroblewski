/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt {
private:
	int mX;
	int mY;

public:

	bool operator==(const Punkt & pkt) const {
		if (pkt.getX() == this->getX() && pkt.getY() == this->getY()) {
			return true;
		} else {
			return false;
		}
	}

//	_zwracany_typ_ operator *( const _typ_ & ); - schemat przeciążenia operatora

	Punkt operator *(const int multi) const {
		Punkt wynik(0, 0);
		wynik.setX(mX * multi);
		wynik.setY(mY * multi);

		return wynik;
	}

	Punkt(int x, int y) :
			mX(x), mY(y) {
	}

	int getX() const {
		return mX;
	}

	void setX(int x) {
		mX = x;
	}

	int getY() const {
		return mY;
	}

	void setY(int y) {
		mY = y;
	}
};

Punkt operator *(const float multi, const Punkt & pkt) {
	return pkt * multi;
}

#endif /* PUNKT_HPP_ */
