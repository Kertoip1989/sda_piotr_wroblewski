/*
 * Caffee.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CAFFEE_HPP_
#define CAFFEE_HPP_

#include "Liquid.hpp"

class Caffee: public Liquid {
protected:
	int mCaffeine;
public:

	Caffee(int amount, int caffeine) :
			Liquid(amount), mCaffeine(caffeine) {
	}

	virtual void add(int amount) {
		mAmount += amount;
	}

	virtual void remove(int amount) {
		mAmount = (mAmount > amount) ? mAmount - amount : 0;
	}

	virtual void removeAll() {
		mAmount = 0;
	}

	virtual ~Caffee() {
	}

};

#endif /* CAFFEE_HPP_ */
