/*
 * Espresso.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef ESPRESSO_HPP_
#define ESPRESSO_HPP_

#include "Caffee.hpp"

class Espresso: public Caffee {
public:
	Espresso(int amount) :
			Caffee(amount, 30) {
	}

	void add(int amount) {
		mAmount += amount;
	}

	void remove(int amount) {
		mAmount = (mAmount > amount) ? mAmount - amount : 0;
	}

	void removeAll() {
		mAmount = 0;
	}

};



#endif /* ESPRESSO_HPP_ */
