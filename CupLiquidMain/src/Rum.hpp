/*
 * Rum.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef RUM_HPP_
#define RUM_HPP_

#include "Liquid.hpp"

enum Colour {
	DARK, LIGHT
};

class Rum: public Liquid {
protected:
	Colour mColour;
public:

	Rum(int amount, Colour colour) :
			Liquid(amount), mColour(colour) {
	}

	void add(int amount) {
		mAmount = (mAmount > amount) ? mAmount - amount : 0;
	}

	void remove(int amount) {
		mAmount -= amount;
	}

	void removeAll() {
		mAmount = 0;
	}
};

#endif /* RUM_HPP_ */
