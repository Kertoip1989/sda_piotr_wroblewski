//============================================================================
// Name        : StrukturaLiczbyMain.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

struct UserInputData {
	int iloscLiczb;
	float min;
	float max;
	bool czyRosnaco;
};

UserInputData pobierzDane() {
	UserInputData uid;

	cout << "Ile liczb? ";
	cin >> uid.iloscLiczb;
	cout << "Min?";
	cin >> uid.min;
	cout << "Max?";
	cin >> uid.max;
	cout << "Rosnaco?";
	cin >> uid.czyRosnaco;

	return uid;
}

void wylosuj(float* tab, int ileLiczb, float min, float max) {

}

void sortuj(float*tab, bool czyRosnąco) {

}

int main() {

	UserInputData daneWejsciowe = pobierzDane();

	float* tab = new float[daneWejsciowe.iloscLiczb];
	wylosuj(tab, daneWejsciowe.iloscLiczb, daneWejsciowe.min,
			daneWejsciowe.max);
	sortuj(tab, daneWejsciowe.czyRosnaco);

	cout << "Ile " << daneWejsciowe.iloscLiczb << " Min " << daneWejsciowe.min
			<< " Max " << daneWejsciowe.max << " Czy rosnaco? "
			<< daneWejsciowe.czyRosnaco << endl;

	return 0;
}
