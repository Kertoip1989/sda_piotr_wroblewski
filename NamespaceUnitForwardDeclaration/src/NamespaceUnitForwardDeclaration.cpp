//============================================================================
// Name        : NamespaceUnitForwardDeclaration.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Ansi-style
//============================================================================

#include <iostream>
#include "Unit.hpp"
#include "Group.hpp"
#include <cstdlib> /*srand, rand */
#include <ctime>

int main() {
	srand(time(NULL));

	Group armyOne;
	Unit* motherOne = new Unit("1");

	armyOne.add(motherOne);
	motherOne->addToGroup(&armyOne);

	Unit* motherTwo = new Unit("2");
	armyOne.add(motherTwo);
	motherTwo->addToGroup(&armyOne);

	armyOne.printUnits();
	armyOne.replicateGroup();
	armyOne.printUnits();
	armyOne.replicateGroup();
	armyOne.printUnits();


	return 0;
}
