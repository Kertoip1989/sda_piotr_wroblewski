/*
 * Group.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef GROUP_HPP_
#define GROUP_HPP_

#include <iostream>
#include <string>

class Unit;

class Group {
private:
	Unit** mUnits;
	int mSize;
	void resize();

public:
	Group():mUnits(0),mSize(0){}

	void add(Unit*);
	void clear();

	void replicateGroup();
	void printUnits();

	~Group();
};

#endif /* GROUP_HPP_ */
