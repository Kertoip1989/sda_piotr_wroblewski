/*
 * Unit.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#include "Unit.hpp"
#include "Group.hpp"
#include <cstdlib> /*srand, rand */
#include <ctime>

void Unit::addToGroup(Group* group) {
	mGroupPtr = group;
}

void Unit::print() {
	std::cout << mId << std::endl;
}

void Unit::replicate() {
	int idVariation = rand() % 10;
	char newId = '0' + idVariation;
	Unit* newborn = new Unit(mId + newId);
	newborn->addToGroup(mGroupPtr);
	mGroupPtr->add(newborn);
};

