/*
 * Unit.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef UNIT_HPP_
#define UNIT_HPP_

#include <iostream>
#include <string>

class Group;

class Unit {
private:
	std::string mId;
	Group* mGroupPtr;
public:
	Unit(std::string id)
:mId(id), mGroupPtr(0){}

	void addToGroup(Group* group);

	void print();

	void replicate();


};


#endif /* UNIT_HPP_ */
