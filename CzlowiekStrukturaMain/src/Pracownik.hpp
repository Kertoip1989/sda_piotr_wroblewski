/*
 * Pracowniek.hpp
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef PRACOWNIK_HPP_
#define PRACOWNIK_HPP_

#include "Czlowiek.hpp"

struct Pracownik : Czlowiek
{
	float pensja;
	int staz;

	void wyliczPensje()
	{
		pensja *= (1+ (++staz)*0.05f);
	}


};



#endif /* PRACOWNIK_HPP_ */
