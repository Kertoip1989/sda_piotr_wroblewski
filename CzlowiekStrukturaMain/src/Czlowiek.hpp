/*
 * Czlowiek.hpp
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef CZLOWIEK_HPP_
#define CZLOWIEK_HPP_

struct Czlowiek {
	int dzienUr;
	int miesiacUr;
	int rokUr;

//	Czlowiek(int dzien, int miesiac, int rok) :
//			dzienUr(dzien), miesiacUr(miesiac), rokUr(rok) {
//	}

	int podajWiek(int rok) {
		return (rok - rokUr < 0) ? 0 : rok - rokUr;
	}

};

#endif /* CZLOWIEK_HPP_ */
