/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt {
	float mX;
	float mY;

public:

	Punkt(float x, float y) :
			mX(x), mY(y) {
	}

	Punkt() :
			mX(0), mY(0) {

	}

	Punkt(const Punkt &pkt) :
			mX(pkt.getX()), mY(pkt.getY()) {
	}

	Punkt operator *(const float multi) const {
		Punkt wynik(0, 0);
		wynik.setX(mX * multi);
		wynik.setY(mY * multi);

		return wynik;
	}

	Punkt operator *(const Punkt pkt2) const {
		Punkt wynik(0, 0);
		wynik.setX(mX * pkt2.getX());
		wynik.setY(mY * pkt2.getY());

		return wynik;
	}

	Punkt operator+(const float flo) const {
		Punkt wynik(0, 0);
		wynik.setX(mX + flo);
		wynik.setY(mY + flo);

		return wynik;
	}

//	_zwracany_typ_ operator +( const _typ_ & );

	Punkt operator+(const Punkt pkt) const {
		Punkt wynik(0, 0);
		wynik.setX(mX + pkt.getX());
		wynik.setY(mY + pkt.getY());

		return wynik;
	}

	float getX() const {
		return mX;
	}

	void setX(float x) {
		mX = x;
	}

	float getY() const {
		return mY;
	}

	void setY(float y) {
		mY = y;
	}

	void wypisz() {
		std::cout << "p.x = " << getX() << ", " << "p.y = " << getY()
				<< std::endl;
	}

	Punkt& operator++()
		{
		this->setX(++mX);
		this->setY(++mY);

		return *this;
		}

	Punkt operator++(int)
		{
		Punkt tmp(*this);
		this->operator++();

		return tmp;

		}

};

Punkt operator +(const float k, const Punkt & pkt) {
	return pkt + k;
}

Punkt operator *(const float multi, const Punkt & pkt) {
	return pkt * multi;
}

#endif /* PUNKT_HPP_ */
