//============================================================================
// Name        : PrzeciazenieOperatorowZadaniePunktMain.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Punkt.hpp"
#include "Zbior.hpp"

using namespace std;

int main() {

	Punkt p1(3, 6);
	Punkt p2(2, -2);

	p1.wypisz();
	p2.wypisz();

	Punkt p3(p1 + p2);
	p3.wypisz();

	Punkt p4(p1 + 2.1);
	p4.wypisz();

	Punkt p5(p1 * 2.3);
	p5.wypisz();

	Punkt p6(p1 * p2);
	p6.wypisz();

	cout << "------------------------" << endl;

	Zbior zbior;

	if (zbior) {
			cout << "Prawidłowy" << endl;
		} else {
			cout << "Nieprawidłowy" << endl;
		}

	cout << "------------------------" << endl;

	zbior + p1;
	zbior + p2;
	zbior + p3;
	zbior.wypisz();

	cout << "------------------------" << endl;

	if (zbior) {
		cout << "Prawidłowy" << endl;
	} else {
		cout << "Nieprawidłowy" << endl;
	}

	cout << "------------------------" << endl;

	Zbior zbiornik;
	zbiornik + Punkt(33, 33);
	zbior + zbiornik;
	zbior.wypisz();

	cout << "------------------------" << endl;

	p1.wypisz();
	++p1;
	p1.wypisz();

	cout << "------------------------" << endl;

	p1.wypisz();
	Punkt p7 = ++p1 +3;
	p7.wypisz();


	return 0;
}
