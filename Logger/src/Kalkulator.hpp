/*
 * Kalkulator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef KALKULATOR_HPP_
#define KALKULATOR_HPP_
#include "FileLogger.hpp"

class Kalkulator {
public:
	int podziel(int a, int b) {
		FileLogger::getInstance()->log("Kalkulator, podziel");
		if (b == 0) {
			FileLogger::getInstance()->log("Nie dzieli si� przez 0");
			return 0;
		} else {
			return a / b;
		}
	}
};

#endif /* KALKULATOR_HPP_ */
