//============================================================================
// Name        : ZwrocMaksymalnaMain.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int size = 10;
	int tablica[size] = { 0, 4, 2, 4, 5, 6, 3, 1, 1, 2 };

	int max = tablica[0];

	for (int i = 0; i < size - 1; i++) {
//		if (tablica[i] > max) {
//			max = tablica[i];
//		}

		max = (tablica[i] > max) ? tablica[i] : max; // operator warunkowy = czesc z if'em okomentowana
	}

	cout << "Wartosc maksymalna = " << max << endl;

	return 0;
};
